<?php

use App\Role;
use App\TicketEntity;
use App\User;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['middleware' => ['not.auth']], function () {
    Route::view("login", "Auth.login");
    Route::post("login", "Auth\AuthController@auth");
});
Route::group(['middleware' => ['auth']], function () {
    Route::get("logout", "Auth\AuthController@logOut");
    Route::view("/", "Panel.index");
    User::crudRoutes();
    Role::crudRoutes();
    TicketEntity::crudRoutes();
    Route::get("test", function () {
        return \App\Facades\Hook::getHooks("create", "User");
    });
});
