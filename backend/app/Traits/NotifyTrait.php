<?php


namespace App\Traits;

use App\User;


trait NotifyTrait
{
    public static function sendTelegramNotify($text, $userId)
    {
        $user = User::findOrFail($userId);
        $post = [
            "token" => md5("IamAliAkbar"),
            "text" => $text,
            "username" => $user->telegram];
        $ch = curl_init("Url");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

}
