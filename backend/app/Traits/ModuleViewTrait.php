<?php


namespace App\Traits;


trait ModuleViewTrait
{
    public static function view($view)
    {

        return \Illuminate\Support\Facades\View::file(self::dir() . "/views/$view.blade.php");
    }
}
