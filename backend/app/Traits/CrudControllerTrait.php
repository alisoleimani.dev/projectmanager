<?php


namespace App\Traits;


use App\Facades\Hook;
use App\Helpers\SubmitResponse;
use Illuminate\Http\Request;

trait CrudControllerTrait
{
    public $namespace = self::NAMESPACE;
    public $slug = self::SLUG;

    public function index()
    {
        ${$this->slug . "s"} = $this->namespace::get();
        return view("Panel." . ucfirst($this->slug) . ".index", compact(self::SLUG . "s"));
    }

    public function store(Request $request)
    {
        return $this->namespace::store($request->all());


    }

    public function show(Request $request)
    {
        ${$this->slug} = $this->namespace::find($request->route($this->slug));
        if (!${$this->slug})
            return abort(404);
        Hook::runHook(ucfirst(self::SLUG), "show", ${$this->slug});
        return view("Panel." . ucfirst($this->slug) . ".show", compact($this->slug));
    }

    public function edit(Request $request)
    {
        ${$this->slug} = $this->namespace::find($request->route($this->slug));
        if (!${$this->slug})
            return abort(404);
        return view("Panel." . ucfirst($this->slug) . ".edit", compact($this->slug));
    }

    public function update(Request $request)
    {
        return $this->namespace::autoUpdate($request->all());
    }

    public function delete(Request $request)
    {
        ${$this->slug} = $this->namespace::find($request->route($this->slug));
        if (!${$this->slug})
            return SubmitResponse::error(__("NotPossibleRequest"));
        ${$this->slug}->delete();
        return back();
    }
}
