<?php


namespace App\Traits;

use App\Facades\Hook;
use App\Helpers\I18nHelper;
use App\Helpers\SubmitResponse;
use Illuminate\Support\Facades\Route;

trait CrudTrait
{
    public static function store(array $data, $returnType = "response")
    {
        $errors = self::checkUniqueErrors($data);
        if ($errors) {
            $response = self::errorsToString($errors);
            return SubmitResponse::error(__("FoundInDb", ["response" => $response]));
        }
        try {
            $store = self::create($data);
            Hook::runHook(self::EN_SLUG, "create", $store);
            if ($returnType == "response")
                return SubmitResponse::toastRedirect(__("SuccessCreate", ["model" => self::getSlug()]), strtolower(self::EN_SLUG) . "/create");
            return $store;
        } catch (\Exception $exception) {
            if ($returnType == "response")
                return SubmitResponse::error(__("ProcessFailed"));
            return false;
        }
    }

    public static function checkUniqueErrors($data)
    {
        $errors = null;
        $i = 0;
        foreach (self::$unique as $key => $slug) {
            if (self::checkUnique($key, $data[$key])) {
                $errors["unique"][$i] = $slug;
                $i++;
            }
        }
        return $errors;
    }

    protected static function checkUnique($key, $value)
    {
        return self::withTrashed()->where($key, $value)->exists();
    }

    protected static function errorsToString($errors)
    {
        $fields = null;
        $i = 1;
        $len = sizeof($errors["unique"]);
        foreach ($errors["unique"] as $error) {
            if ($i + 1 == $len)
                $fields .= $error . " " . __("and") . " ";
            else if ($i == $len)
                $fields .= $error;
            else
                $fields .= $error . "، ";
            $i++;
        }
        if ($i == 2)
            $filedWord = __("Field");
        else
            $filedWord = __("Fields");
        return $filedWord . " " . $fields;
    }

    public static function getAll()
    {
        return self::get();
    }

    public static function crudRoutes()
    {
        Route::group(["prefix" => strtolower(self::EN_SLUG)], function () {
            Route::get("", self::EN_SLUG . "\\" . self::EN_SLUG . "Controller@index")->middleware("permission:" . self::EN_SLUG . ".index");
            Route::get("show/{" . strtolower(self::EN_SLUG) . "}", self::EN_SLUG . "\\" . self::EN_SLUG . "Controller@show")->middleware("permission:" . self::EN_SLUG . ".show");
            Route::view("create", "Panel." . self::EN_SLUG . ".create")->middleware("permission:" . self::EN_SLUG . ".create");
            Route::post("store", self::EN_SLUG . "\\" . self::EN_SLUG . "Controller@store")->middleware("permission:" . self::EN_SLUG . ".create");
            Route::get("edit/{" . strtolower(self::EN_SLUG) . "}", self::EN_SLUG . "\\" . self::EN_SLUG . "Controller@edit")->middleware("permission:" . self::EN_SLUG . ".edit");
            Route::post("edit/{" . strtolower(self::EN_SLUG) . "}", self::EN_SLUG . "\\" . self::EN_SLUG . "Controller@update")->middleware("permission:" . self::EN_SLUG . ".edit");
            Route::get("delete/{" . strtolower(self::EN_SLUG) . "}", self::EN_SLUG . "\\" . self::EN_SLUG . "Controller@delete")->middleware("permission:" . self::EN_SLUG . ".delete");
            if (method_exists(self::class, "additionalRoutes"))
                self::additionalRoutes();
        });
    }

    public static function autoUpdate(array $data, $redirectTo = null, $returnType = "response")
    {

        $object = self::find($data["id"]);
        $errors = self::checkUpdateUniqueErrors($data, $object);
        if ($errors) {
            $response = self::errorsToString($errors);
            return SubmitResponse::error(__("FoundInDb", ["response" => $response]));
        }
        $data = (object)$data;
        $updateFields = self::$updatable;
        foreach ($updateFields as $field) {
            $object->$field = $data->$field;
        }
        try {
            $object->save();
            Hook::runHook(self::EN_SLUG, "create", $object);
            if (!$redirectTo)
                $url = strtolower(self::EN_SLUG) . "/edit/" . $data->id;
            else
                $url = $redirectTo;
            if ($returnType == "response")
                return SubmitResponse::toastRedirect(__("SuccessEdit", ["model" => self::getSlug()]), $url);
            return $object;
        } catch (\Exception $exception) {

            return SubmitResponse::error(__("ProcessFailed"));
        }
    }

    public static function checkUpdateUniqueErrors($data, $object)
    {
        $errors = null;
        $i = 0;
        foreach (self::$unique as $key => $slug) {
            if ($data[$key] != $object->$key) {
                if (self::checkUnique($key, $data[$key])) {
                    $errors["unique"][$i] = $slug;
                    $i++;
                }
            }
        }
        return $errors;
    }

    public static function getSlug()
    {
        return I18nHelper::setSlug(["en" => self::EN_SLUG, "fa" => self::FA_SLUG]);
    }
}
