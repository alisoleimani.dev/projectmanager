<?php

namespace App;

use App\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;


class User extends Model
{
    use CrudTrait;
    use SoftDeletes;

    protected const FA_SLUG = "کاربر";
    protected const EN_SLUG = "User";
    protected static $unique = ["email" => "ایمیل"];
    protected static $updatable = ["email", "name", "telegram"];
    protected $fillable = ['name', 'email', 'password', 'role_id', "telegram"];
    protected $hidden = ['password'];

    protected static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            $model->password = sha1($model->password);
        });
    }

    public static function getSessionUser()
    {
        $user = Session::get("user");
        if ($user) {
            $user = User::find($user->id);
            Session::put("user", $user);
            return $user;
        }
        return null;
    }

    public static function authCheck()
    {
        if (self::getSessionUser())
            return true;
        return false;
    }

    public static function auth(object $data)
    {
        $user = self::whereEmail($data->email)->wherePassword(sha1($data->password))->first();
        if ($user) {
            Session::put("user", $user);
            return true;
        }
        return false;
    }

    public static function logOut()
    {
        Session::remove("user");
    }

    public function avatar()
    {
        $dir = url('/backend/public/');
        if ($this->avatar == "" || $this->avatar == null)
            return $dir . "/" . "img/default.jpg";
        return $dir . "/" . $this->avatar;
    }

    public function role()
    {
        return $this->belongsTo("App\Role")->withTrashed();
    }

    public static function additionalRoutes()
    {
        Route::get("profile", "User\UserController@profile");
        Route::get("profile/edit", "User\UserController@profileEdit");
        Route::post("profile/edit", "User\UserController@profileUpdate");
        Route::post("profile/avatar", "User\UserController@avatarUpdate");
        Route::post("profile/password", "User\UserController@passwordUpdate");
    }

    public function permissions()
    {
        return $this->belongsToMany(Permission::class, "user_permissions")->withPivot("permission");
    }

    public static function can($permission)
    {
        return Permission::can($permission, self::getSessionUser());
    }


}

