<?php


namespace App\Classes;


abstract class ModuleAbstract
{

    abstract public function __construct();

    abstract static public function dir();

    abstract public function activate();

    abstract public function deActivate();

    abstract public function index();

    abstract public static function subMenu(): array;
}
