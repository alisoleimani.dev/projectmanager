<?php


namespace App\Classes;


class HookManager
{
    private $hooks = ["create" => [], "edit" => [], "delete" => [], "show" => [], "adminHeader" => []];


    public function onCreate($model, $callback, $namespace = null)
    {
        array_push($this->hooks["create"], ["model" => ucfirst($model), "callback" => $callback, "namespace" => $namespace]);
        return $this;
    }

    public function onEdit($model, $callback, $namespace = null)
    {
        array_push($this->hooks["edit"], ["model" => ucfirst($model), "callback" => $callback, "namespace" => $namespace]);
        return $this;
    }

    public function onShow($model, $callback, $namespace = null)
    {
        array_push($this->hooks["show"], ["model" => ucfirst($model), "callback" => $callback, "namespace" => $namespace]);
        return $this;
    }

    public function onDelete($model, $callback, $namespace = null)
    {
        array_push($this->hooks["delete"], ["model" => ucfirst($model), "callback" => $callback, "namespace" => $namespace]);
        return $this;
    }

    public function adminHeader($callback, $namespace = null)
    {
        array_push($this->hooks["adminHeader"], ["callback" => $callback, "namespace" => $namespace]);
        return $this;
    }

    public function runHook($onModel, $hookAction, $params = null)
    {

        $hooks = $this->getHooks($hookAction, $onModel);
        foreach ($hooks as $hook) {
            if ($this->isFunction($hook['namespace'], $hook['callback'])) {
                $hook->callback($params);

            }
            $instance = $this->createInstance($hook["namespace"]);
            $action = $hook["callback"];
            $instance->$action($params);
        }
    }

    public function getHooks($type = null, $model = null)
    {
        if ($type && $model) {
            $modelArray = [];
            foreach ($this->hooks[$type] as $hook) {
                if ($hook['model'] == $model)
                    array_push($modelArray, $hook);
            }
            return $modelArray;
        }
        if ($type)
            return $this->hooks[$type];
        if ($model)
            throw new \Exception("Type of Hook Not Selected");
        return $this->hooks;
    }

    private function isFunction($namespace, $callback)
    {
        if (!$namespace)
            if (function_exists($callback))
                return true;
            else
                throw new \Exception("Function $callback not Exists");
        else
            return false;

    }

    private function createInstance($namespace)
    {
        if (!class_exists($namespace))
            throw new \Exception("Class $namespace not Exists");
        return new $namespace();
    }
}
