<?php


namespace App\Classes;


use App\Module;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Support\Facades\Storage;


final class ModuleManager
{
    private const MODULE_DIR = "modules";
    private $modules;
    private const SLASH = "/";
    private const BACK_SLASH = "\\";
    private const PHP_EXTENSION = ".php";
    private const CONFIG_FILE = "/config.json";
    private const MAIN_CLASS = "Main";

    public function __construct()
    {
        $this->modules = $this->getModules();
        $this->runModules();
    }

    public static function getModules()
    {
        return Storage::disk(self::MODULE_DIR)->directories();
    }

    private function getModulePath($module)
    {
        return
            base_path() .
            self::SLASH . self::MODULE_DIR . self::SLASH . $module .
            self::SLASH . self::MAIN_CLASS . self::PHP_EXTENSION;
    }

    private function runModules()
    {
        foreach ($this->modules as $module) {
            if ($this->moduleDbCheck($module)) {
                $this->includeModule($this->getModulePath($module));
                $this->newInstanceModule(ucfirst($module) . self::BACK_SLASH . self::MAIN_CLASS);
            }
        }
    }

    private function includeModule($path)
    {
        include_once "$path";
    }

    private function newInstanceModule($ModuleClassName)
    {
        $ModuleClassName = "Module\\" . $ModuleClassName;

        return new $ModuleClassName();
    }

    public static function isModel($model)
    {
        if (class_exists("App\\" . ucfirst($model)))
            return true;
        return false;
    }

    public function getModulesNames()
    {
        $names = [];
        foreach ($this->modules as $module) {
            if ($this->getModuleName($module) != null)
                $names[$module] = $this->getModuleName($module);
        }
        return $names;
    }

    private function getModuleName($module)
    {
        if ($this->moduleConfig($module) != null)
            return json_decode($this->moduleConfig($module))->name;
        else
            return null;
    }

    private function moduleConfig($module)
    {
        try {
            return Storage::disk(self::MODULE_DIR)->get($module . self::CONFIG_FILE);
        } catch (FileNotFoundException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    private function configExists($module)
    {
        return Storage::disk(self::MODULE_DIR)->exists($module . self::CONFIG_FILE);

    }

    private function moduleDbCheck($module)
    {

        $moduleModel = Module::whereName($module)->first();
        if (!$moduleModel) {
            if ($this->configExists($module))
                Module::create(["name" => $module, "status" => Module::DISABLE]);

            return false;
        }
        if ($moduleModel->status == Module::ENABLE) {
            return true;
        }
        return false;
    }


}

