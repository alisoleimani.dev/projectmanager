<?php

namespace App\Http\Middleware;

use App\User;
use Closure;

class NotAuth
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (User::authCheck())
            return redirect("/");
        return $next($request);
    }
}
