<?php

namespace App\Http\Middleware;

use App\User;
use Closure;

class PermissionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @param $permission
     * @return mixed
     */
    public function handle($request, Closure $next, $permission)
    {
        $user = User::getSessionUser();
        if ($user->can($permission))
            return $next($request);
        return abort(403, "Access Denied");
    }
}
