<?php

namespace App\Http\Middleware;

use App\Helpers\SubmitResponse;
use App\User;
use Closure;

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (User::authCheck())
            return $next($request);
        if ($request->method() == "GET")
            return redirect("/login");
        return SubmitResponse::jsonResponse(["error" => "Unauthorized Action"], 401);
    }
}
