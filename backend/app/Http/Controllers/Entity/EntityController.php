<?php

namespace App\Http\Controllers\Entity;

use App\Http\Controllers\Controller;
use App\Traits\CrudControllerTrait;

class EntityController extends Controller
{
    use CrudControllerTrait;

    public const NAMESPACE = "App\Entity";
    public const SLUG = "entity";
}
