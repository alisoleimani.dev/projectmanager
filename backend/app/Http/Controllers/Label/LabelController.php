<?php

namespace App\Http\Controllers\Label;

use App\Http\Controllers\Controller;
use App\Traits\CrudControllerTrait;

class LabelController extends Controller
{
    use CrudControllerTrait;

    public const NAMESPACE = "App\Label";
    public const SLUG = "label";
}
