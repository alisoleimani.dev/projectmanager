<?php

namespace App\Http\Controllers\User;

use App\Helpers\SubmitResponse;
use App\Helpers\UploadHelper;
use App\Http\Controllers\Controller;
use App\Traits\CrudControllerTrait;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    use CrudControllerTrait;

    public const NAMESPACE = "App\User";
    public const SLUG = "user";

    public function avatarUpdate(Request $request)
    {
        $user = User::getSessionUser();
        $uploadAvatar = new UploadHelper();
        $uploadAvatar = $uploadAvatar->files($request->file("avatar"))->handle();
        if ($uploadAvatar->status) {
            $user->avatar = $uploadAvatar->name;
            $user->save();
            return SubmitResponse::toastRedirect(__("SuccessAvatarChange"), "user/profile/edit");
        } else
            return SubmitResponse::error($uploadAvatar->error);
    }

    public function passwordUpdate(Request $request)
    {
        $user = User::getSessionUser();
        if ($request->new_password !== $request->repeat_password)
            return SubmitResponse::error(__("NewPasswordNotMatch"));
        if ($user->password !== sha1($request->password))
            return SubmitResponse::error(__("CurrentPasswordNotMatch"));
        $user->password = sha1($request->new_password);
        $user->save();
        return SubmitResponse::toastRedirect(__("SuccessEdit", ["model" => __("Password")]), "user/profile/edit");
    }

    public function profile()
    {
        $user = User::getSessionUser();
        $myProfile = true;
        return view("Panel.User.show", compact("user", "myProfile"));
    }

    public function profileEdit()
    {
        $user = User::getSessionUser();
        return view("Panel.User.profile", compact("user"));
    }

    public function profileUpdate(Request $request)
    {
        $user = User::getSessionUser();
        if ($user->id != $request->id)
            return SubmitResponse::error("Access Denied");
        return User::autoUpdate($request->all(), "user/profile/edit");


    }
}
