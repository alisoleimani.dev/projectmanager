<?php

namespace App\Http\Controllers\Role;

use App\Http\Controllers\Controller;
use App\Traits\CrudControllerTrait;


class RoleController extends Controller
{
    use CrudControllerTrait;
    public const NAMESPACE = "App\Role";
    public const SLUG = "role";
}
