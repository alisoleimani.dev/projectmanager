<?php

namespace App\Http\Controllers\Auth;

use App\Helpers\SubmitResponse;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class AuthController extends Controller
{

    public function auth(Request $request)
    {
        if (User::auth($request))
            return SubmitResponse::toastRedirect(__("SuccessLogin"));
        return SubmitResponse::error(__("FailedLogin"));
    }

    public function logOut()
    {
        User::logOut();
        return redirect("/");
    }
}
