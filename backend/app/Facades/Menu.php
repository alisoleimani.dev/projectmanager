<?php


namespace App\Facades;

use App\Helpers\ViewHelper;
use Illuminate\Support\Facades\Facade;

/**
 * @method static ViewHelper crudMainMenu()
 * @method static ViewHelper addMenu($permission, $slug)
 *
 * @see ViewHelper
 */
class Menu extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'menu';
    }
}
