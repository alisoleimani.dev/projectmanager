<?php


namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class Hook extends Facade
{
    /**
     * @method static App\Classes\HookManager onCreate($model, $callback, $namespace = null)
     * @method static App\Classes\HookManager onEdit($model, $callback, $namespace = null)
     * @method static App\Classes\HookManager onDelete($model, $callback, $namespace = null)
     * @method static App\Classes\HookManager onShow($model, $callback, $namespace = null)
     * @method static App\Classes\HookManager adminHeader($callback, $namespace = null)
     * @method static App\Classes\HookManager getHooks($type = null, $model = null)
     * @method static App\Classes\HookManager runHook($onModel, $hookAction, $params)
     *
     *
     * @see App\Classes\HookManager
     */
    protected static function getFacadeAccessor()
    {
        return 'hook';
    }
}
