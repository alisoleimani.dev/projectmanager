<?php

namespace App;

use App\Traits\CrudTrait;
use App\Traits\NotifyTrait;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    use CrudTrait;
    use NotifyTrait;

    public function user()
    {
        return $this->hasOne(User::class);
    }
}
