<?php


namespace App\Interfaces;


interface PermissionInterface
{
    public const ACCESS = "access";
    public const NOACCESS = "no_access";
    public const INHERITANCE = "inheritance";

}
