<?php

namespace App\Helpers;

class UploadHelper
{

    public $upload;
    /**
     * @var array
     */
    private $formats = ['png', 'jpg', 'jpeg'];
    /**
     * @var float|int
     */
    private $size = 1024 * 1000;
    /**
     * @var string
     */
    private $dir = "Uploads";
    private $files;

    public function files($files)
    {
        $this->files = $files;
        return $this;
    }

    public function formats($formats)
    {
        $this->formats = array_map('strtolower', $formats);
        return $this;
    }

    public function maxSize($size)
    {
        $this->size = $size * pow(10, 3);
        return $this;
    }

    public function dir($dir)
    {
        $this->dir = $dir;
        return $this;
    }

    public function handle()
    {
        $i = 0;
        if (!is_array($this->files))
            $this->files = [$this->files];
        foreach ($this->files as $file) {
            $format = strtolower($file->extension());
            if (in_array($format, $this->formats)) {
                if ($file->getSize() < $this->size) {
                    $filename = time() . $file->getClientOriginalName();
                    if ($file->move(public_path($this->dir), $filename)) {
                        $this->upload[$i]['status'] = true;
                        $this->upload[$i]['url'] = url('') . "/backend/public/$this->dir/$filename";
                        $this->upload[$i]['name'] = $this->dir . "/" . $filename;
                    } else {
                        $this->upload[$i]['status'] = false;
                        $this->upload[$i]['error'] = __("UploadNotPossible");
                    }
                } else {
                    $this->upload[$i]['status'] = false;
                    $this->upload[$i]['error'] = __("UploadSizeError");
                }
            } else {
                $this->upload[$i]['status'] = false;
                $this->upload[$i]['error'] = __("UploadFormatError");
            }
            $i++;
        }
        if ($i == 1)
            return (object)$this->upload[0];
        return $this->upload;
    }

}
