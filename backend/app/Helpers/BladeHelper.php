<?php


namespace App\Helpers;


use App\User;
use Illuminate\Support\Facades\Blade;

class BladeHelper extends Blade
{
    public static function publicUrl()
    {
        self::directive("publicUrl", function () {
            return url('/backend/public');
        });
    }

    public static function url()
    {
        self::directive("URL", function () {
            return url("/");
        });
    }

    public static function pic()
    {
        self::directive("pic", function ($src) {
            $picUrl = url('/backend/public/') . "/" . $src;
            ?>
            <?= $picUrl ?>
            <?php
        });
    }

    public static function composerView()
    {
        view()->composer('*', function ($view) {
            $sessionUser = User::getSessionUser();
            $view->with('sessionUser', $sessionUser);
        });
    }

    public static function menuAttach()
    {
        view()->composer('assets.menu', function ($view) {
            $viewHelper = new ViewHelper();
            $view->with('viewHelper', $viewHelper);
        });
    }
}
