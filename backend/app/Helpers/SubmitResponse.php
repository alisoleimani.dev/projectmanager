<?php


namespace App\Helpers;

class SubmitResponse
{
    private const ACTION_ALERT = "ACTION_ALERT";
    private const REDIRECT = "REDIRECT";
    private const REFRESH = "REFRESH";
    private $response;

    public function __construct(bool $status, string $msg)
    {
        $this->response = ["status" => $status, "msg" => $msg];
        return $this;
    }

    public static function toastRedirect($msg, $uri = "", $color = "success")
    {
        $response = new self(true, $msg);
        return $response->actionAlert("toastr", $uri, $color)->send();
    }

    public function send()
    {
        return response()->json($this->response);
    }

    public function actionAlert($mode, $uri, $color)
    {
        $this->setMode($mode);
        $this->setColor($color);
        $this->setUri($uri);
        $this->setAction(self::ACTION_ALERT);
        return $this;
    }

    public function setMode($mode)
    {
        $this->response["mode"] = $mode;
        return $this;
    }

    public function setColor($color)
    {
        $this->response["color"] = $color;
        return $this;
    }

    public function setUri($uri)
    {
        $this->response["url"] = $uri;
        return $this;
    }

    public function setAction($action)
    {
        $this->response["action"] = $action;
        return $this;
    }

    public static function swalRedirect($msg, $uri = "", $color = "success")
    {
        $response = new self(true, $msg);
        return $response->actionAlert("swal", $uri, $color)->send();
    }

    public static function error($msg)
    {
        $response = new self(false, $msg);
        return $response->send();
    }

    public static function jsonResponse(array $data, $responseCode = 200)
    {
        $response["type"] = "json";
        foreach ($data as $key => $value) {
            $response[$key] = $value;
        }
        return response()->json($response, $responseCode);
    }

    public function redirectAction($uri)
    {
        $this->setUri($uri);
        $this->setAction(self::REDIRECT);
    }

    public function refreshAction()
    {
        $this->setAction(self::REFRESH);
    }
}
