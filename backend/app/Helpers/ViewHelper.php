<?php


namespace App\Helpers;


use App\Permission;
use App\User;

class ViewHelper
{
    private $menus = ["User" => ["slug" => "Users", "icon" => "flaticon-user-1"],
        "Role" => ["slug" => "Roles", "icon" => "flaticon-structure"],
        "TicketEntity" => ["slug" => "TicketEntity", "icon" => "flaticon-shapes-1"],
        "Label" => ["slug" => "Labels", "icon" => "flaticon-shapes-1"]];

    public function addMenu($title, $slug)
    {
        $this->menus[$title] = ['slug' => $slug];
    }

    public function crudMainMenu()
    {
        $user = User::getSessionUser();
        foreach ($this->menus as $menu => $attr) {
            if ($user->can($menu)) {
                $attr = (object)$attr;
                ?>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle active-page" href="#" id="<?= __($attr->slug) ?>" role="button"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="icon-devices_other nav-icon"></i>
                        <?= __($attr->slug) ?>
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="<?= __($attr->slug) ?>">
                        <?php ViewHelper::menuItems($menu) ?>
                    </ul>
                </li>

                <?php
            }
        }
    }

    public function crudSubMenu($additional = null)
    {
        $user = User::getSessionUser();
        foreach ($this->menus as $menu => $attr) {
            if ($user->can($menu)) {
                $attr = (object)$attr;
                ?>
                <ul class="list-unstyled" data-link="<?= $menu ?>">
                    <h3 class="text-center mt-4"><?= $attr->slug ?></h3>
                    <?php ViewHelper::menuItems($menu) ?>
                </ul>

                <?php
            }
        }
    }

    public function menuItems($model)
    {
        $user = User::getSessionUser();
        foreach (Permission::getModelMenuPermissions($model) as $permission => $attr) {
            $attr = (object)$attr;
            if ($user->can(Permission::PermissionString($model, $permission))) {
                $url = url("") . "/" . strtolower($attr->uri);
                ?>
                <li>
                    <a class="dropdown-item" href="<?= $url ?>"><?= __($attr->slug) ?>
                    </a>
                </li>
                <?php
            }
        }

    }
}
