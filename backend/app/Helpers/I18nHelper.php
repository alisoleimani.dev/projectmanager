<?php


namespace App\Helpers;


use Illuminate\Support\Facades\App;

class I18nHelper
{
    const EN = "en";
    const FA = "fa";
    const RTL = "rtl";
    const LTR = "ltr";
    const RIGHT = "right";
    const LEFT = "left";

    const TEXT_RIGHT = "text-right";
    const TEXT_LEFT = "text-left";

    public static function setSlug(array $slugs)
    {
        if (isset($slugs[App::getLocale()]))
            return $slugs[App::getLocale()];
        return $slugs[self::EN];
    }

    public static function langDirection()
    {
        if (App::getLocale() == self::EN) {
            return self::LTR;
        }
        if (App::getLocale() == self::FA) {
            return self::RTL;
        }
        return self::LTR;
    }

    public static function bootstrapReverseTextAlign()
    {
        if (App::getLocale() == self::EN) {
            return self::TEXT_RIGHT;
        }
        if (App::getLocale() == self::FA) {
            return self::TEXT_LEFT;
        }
        return self::TEXT_RIGHT;
    }

    public static function textAlign()
    {
        if (App::getLocale() == self::EN) {
            return self::LEFT;
        }
        if (App::getLocale() == self::FA) {
            return self::RIGHT;
        }
        return self::LEFT;

    }
}
