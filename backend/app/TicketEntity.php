<?php

namespace App;

use App\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TicketEntity extends Model
{
    use CrudTrait;
    use SoftDeletes;

    protected const FA_SLUG = "موجودیت";
    protected const EN_SLUG = "TicketEntity";
    protected static $unique = ["slug" => "اسلاگ"];
    protected static $updatable = ["title", "slug", "parent_id", "description"];
    protected $fillable = ["title", "slug", "parent_id", "description"];

    public function parentName()
    {
        if ($this->parent_id == null) {
            return "بدون پدر";
        }
        return self::withTrashed()->where("id", $this->parent_id)->first()->title;
    }
}
