<?php

namespace App\Providers;

use App\Classes\HookManager;
use Illuminate\Support\ServiceProvider;

class HookServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        app()->singleton("hook", function ($app) {
            return new HookManager();
        });

    }
}
