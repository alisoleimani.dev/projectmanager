<?php

namespace App\Providers;

use App\Classes\MenuManager;
use App\Classes\ModuleManager;
use App\Helpers\ViewHelper;
use App\Interfaces\ModuleInterface;
use Illuminate\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {


        $this->app->singleton("menu", function ($app) {
            return new ViewHelper();
        });
        $this->loadViewsFrom(["modules/shop"], "Module/Shop");

        new ModuleManager();


    }
}

