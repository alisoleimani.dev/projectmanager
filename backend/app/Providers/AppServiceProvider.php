<?php

namespace App\Providers;

use App\Helpers\BladeHelper;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        BladeHelper::URL();
        BladeHelper::publicUrl();
        BladeHelper::pic();
        BladeHelper::composerView();
    }


}
