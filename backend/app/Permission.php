<?php

namespace App;

use App\Interfaces\PermissionInterface;
use Illuminate\Database\Eloquent\Model;

class Permission extends Model implements PermissionInterface
{
    protected $fillable = ["name"];

    public static function can($permission, User $user)
    {
        if (!self::whereName($permission)->exists()) {
            $admin = User::find(1);
            if (class_exists("App\\" . $permission)) {
                $createIdArray = self::createMany([["name" => $permission],
                    ["name" => $permission . ".index"],
                    ["name" => $permission . ".show"],
                    ["name" => $permission . ".create"],
                    ["name" => $permission . ".delete"],
                    ["name" => $permission . ".edit"],
                ]);
                $admin->permissions()->syncWithoutDetaching($createIdArray);
            } elseif (class_exists("Module\\Shop\\" . $permission)) {
                $create = self::create(["name" => $permission]);
                $admin->permissions()->syncWithoutDetaching($create->id);
            } else {
                throw new \Exception("Permission $permission Not Valid");
            }
        }
        $permissionCheck = self::whereName($permission)->first();
        $userCheck = $user->permissions()->where("name", $permissionCheck->name)->first();
        if (!$userCheck) {
            $roleCheck = $user->role->permissions()->where("name", $permissionCheck->name)->first();
            if (!$roleCheck)
                return false;
            if ($roleCheck->pivot->permission == self::ACCESS)
                return true;
        }
        if ($userCheck->pivot->permission == self::ACCESS)
            return true;
        return false;

    }

    public static function PermissionString($model, $permission)
    {
        return ucfirst($model) . "." . strtolower($permission);

    }

    public static function getModelMenuPermissions($model)
    {
        $namespace = "App\\$model";
        if (class_exists($namespace)) {
            if (property_exists($namespace, "permissions"))
                return $namespace::$permissions;
        }
        $moduleNamespace = "Module\\$model";
        if (class_exists($moduleNamespace)) {
            if (property_exists($moduleNamespace, "subMenus"))
                return $moduleNamespace::subMenus();
            else
                return [];
        }
        return ["index" => ["uri" => $model, "slug" => __("Show")],
            "create" => ["uri" => $model . "/create", "slug" => __("Add")]];
    }

    private static function createMany(array $array)
    {
        $idArray = [];
        foreach ($array as $subArray) {
            $store = self::create($subArray);
            array_push($idArray, $store->id);
        }
        return $idArray;
    }

    public static function add($name)
    {
        if (self::whereName($name)->exists())
            return;
        $create = self::create(["name" => $name]);
        $admin = User::find(1);
        $admin->permissions()->syncWithoutDetaching($create->id);
        return;
    }


}
