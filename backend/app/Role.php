<?php

namespace App;

use App\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    use CrudTrait;
    use SoftDeletes;
    protected const FA_SLUG = "نقش";
    protected const EN_SLUG = "Role";
    protected static $unique = ["slug" => "اسلاگ"];
    protected static $updatable = ["name", "slug", "parent_id"];
    protected $fillable = ["name", "slug", "parent_id", "admin_id"];


    public function parentName()
    {
        if ($this->parent_id == null) {
            return "بدون پدر";
        }
        return self::withTrashed()->where("id", $this->parent_id)->first()->name;
    }

    public function permissions()
    {
        return $this->belongsToMany(Permission::class, "role_permissions")->withPivot("permission");
    }

}
