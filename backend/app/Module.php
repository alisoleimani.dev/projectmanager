<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    public const ENABLE = "enable";
    public const DISABLE = "disable";
    protected $fillable = ["name", "status"];
}
