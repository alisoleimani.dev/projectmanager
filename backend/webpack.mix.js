const mix = require("laravel-mix");

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.js(
    [
        "public/js/bootstrap.bundle.min.js",
        "public/vendor/slimscroll/slimscroll.min.js",
        "public/vendor/slimscroll/custom-scrollbar.js",
        "public/vendor/rating/raty.js",
        "public/js/main.js",
    ],
    "public/js/apps.js"
);
mix.combine(
    [
        "public/css/bootstrap.min.css",
        "public/fonts/style.css",
        "public/css/main.css",
        "public/css/vendor/daterange/daterange.css",
        "public/css/vendor/chartist/css/chartist.min.css",
        "public/css/vendor/chartist/css/chartist-custom.css",
    ],
    "public/css/apps.css"
);

