@include('assets.styles',["title"=>"Dashboard"])

@include('assets.menu')
<div class="content-wrapper">

    <!-- Row starts -->
    <div class="row gutters">
        <div class="col-xl-4 col-lg-12 col-md-12 col-sm-12 col-12">
            <!-- Row start -->
            <div class="row gutters">
                <div class="col-xl-6 col-lg-4 col-md-4 col-sm-4 col-12">
                    <div class="info-tiles">
                        <div class="info-icon">
                            <i class="icon-account_circle"></i>
                        </div>
                        <div class="stats-detail">
                            <h3>185k</h3>
                            <p>Active Users</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-4 col-md-4 col-sm-4 col-12">
                    <div class="info-tiles">
                        <div class="info-icon">
                            <i class="icon-watch_later"></i>
                        </div>
                        <div class="stats-detail">
                            <h3>450</h3>
                            <p>Active Agents</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-4 col-md-4 col-sm-4 col-12">
                    <div class="info-tiles">
                        <div class="info-icon">
                            <i class="icon-visibility"></i>
                        </div>
                        <div class="stats-detail">
                            <h3>7500</h3>
                            <p>Visitors</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@include('assets.scripts')
</body>
</html>
