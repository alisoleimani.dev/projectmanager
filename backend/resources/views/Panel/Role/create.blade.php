@include('assets.styles',["title"=>__("Create",["model"=>__("Role")])])

@include('assets.menu')
<div class="content-wrapper">
    <div class="row">
        <div class="col-12">
            <h1 class="">{{__("Create",["model"=>__("Role")])}}</h1>
            <nav class="breadcrumb-container d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                <ol class="breadcrumb pt-0">
                </ol>
            </nav>
            <div class="separator mb-5">
            </div>
        </div>
        <div class="col-md-7 mx-auto ">
            <form autofill class="col-md-12">
                <div class="row">
                    <div class="col-md-6 mb-3">
                        <div class="form-group">
                            <label for="name">{{__("NameOf",["model"=>__("Role")])}}</label>
                            <input type="text" class="form-control " id="name" placeholder="">
                        </div>
                    </div>
                    <div class="col-md-6 mb-3">
                        <div class="form-group">
                            <label for="name">{{__("Slug")}}</label>
                            <input type="text" class="form-control" id="name" placeholder="">
                        </div>
                    </div>
                    <div class="col-md-6 mb-3">
                        <div class="form-group">
                            <label for="name">{{__("Parent")}}</label>
                            <select class="form-control select2"
                                    id="parent_id">
                                <option value="">{{__("WithoutParent")}}</option>
                                @foreach(\App\Role::getAll() as $role)
                                    <option value="{{$role->id}}">{{$role->name}}</option>
                                @endforeach
                            </select>
                        </div>

                    </div>

                    <div class="col-md-6 mb-3">
                        <div class="form-group">
                            <label for="admin_id">{{__("Administrator")}}</label>
                            <select class="form-control select2"
                                    id="admin_id">
                                <option value="">{{__("PleaseChoice")}}</option>
                                @foreach(\App\User::getAll() as $user)
                                    <option value="{{$user->id}}">{{$user->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <button class="col-md-2 btn btn-primary mt-2 mx-auto submit" type="submit">
                        {{__("Create",["model"=>"Role"])}}
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
</main>
@include('assets.scripts')
<script>
    $(".submit").click(function (e) {
        e.preventDefault()
        submiter(["#name", "#slug"], "role/store", "POST", "", ["#parent_id"]);
    })
</script>
</body>
</html>
