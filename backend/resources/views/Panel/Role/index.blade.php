<!DOCTYPE html>
<html lang="en">

<head>
    <title>نت افراز | مدیریت نقش کاربری </title>
    @include('assets.styles')
</head>

<body id="app-container" class="menu-default">
@include('assets.menu')
<main>
    <div class="col-12">
        <h1>مدیریت نقش های کاربری </h1>
        <nav class="breadcrumb-container d-sm-block d-lg-inline-block" aria-label="breadcrumb">
            <ol class="breadcrumb pt-0">

            </ol>
        </nav>
        <div class="separator mb-5">
        </div>
    </div>
    <div class="col-md-12">
        <div class="table-responsive">
            <table class="table table-hover col-md-12" dir="rtl">
                <thead>
                <tr>
                    <th>ردیف</th>
                    <th>نام نقش کاربری</th>
                    <th>پدر</th>
                    <th>دسترسی ها</th>
                </tr>
                </thead>
                @foreach($roles as $role)
                    <tr class="w-100">
                        <td class="w-10">{{$role->id}}</td>
                        <td class="w-20">{{$role->name}}</td>
                        <td class="w-20">{{$role->parentName()}}</td>
                        <td><a href="{{url('')}}/role/show/{{$role->id}}">مدیریت</a></td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
</main>
@include('assets.scripts')
</body>
<script>

</script>

</html>
