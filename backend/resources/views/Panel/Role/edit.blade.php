<!DOCTYPE html>
<html lang="en" class="px-2">

<head>
    <title>ویرایش نقش کاربری | نت افراز</title>
    @include('assets.styles')
</head>
<body id="app-container" class="menu-default">
@include('assets.menu')
<main>
    <div class="container-fluid">
        <div class="row" dir="rtl">
            <div class="col-12">
                <h1>ویرایش نقش کاربری</h1>
                <nav class="breadcrumb-container d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                    <ol class="breadcrumb pt-0">
                    </ol>
                </nav>
                <div class="separator mb-5">
                </div>
            </div>
            <div class="col-md-7 mx-auto ">
                <form autofill class="col-md-12">
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <label class="form-group has-float-label">
                                <input class="form-control" value="{{$role->name}}" id="name">
                                <span>عنوان نقش کاربری</span>
                            </label>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label class="form-group has-float-label">
                                <input class="form-control" value="{{$role->slug}}" id="slug">
                                <span>اسلاگ</span>
                            </label>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label class="form-group has-float-label">
                                <select class="form-control select2" id="parent_id">
                                    <option value="">بدون پدر</option>
                                    @foreach(\App\Role::getAll() as $r)
                                        <option value="{{$r->id}}">{{$r->name}}</option>
                                    @endforeach
                                </select>
                                <span>پدر</span>
                            </label>
                        </div>

                        <div class="col-md-6 mb-3">
                            <label class="form-group has-float-label">
                                <select class="form-control select2" id="admin_id">
                                    <option value="">لطفا انتخاب نمایید</option>
                                    @foreach(\App\User::getAll() as $user)
                                        <option value="{{$user->id}}">{{$user->name}}</option>
                                    @endforeach
                                </select>
                                <span>مدیر نقش</span>
                            </label>
                        </div>
                        <button class="col-md-2 btn btn-primary mt-2 mx-auto submit" type="submit">ویرایش
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</main>
@include('assets.scripts')
<script>
    $(".submit").click(function (e) {
        e.preventDefault()
        submiter(["#name", "#slug"], "role/edit/{{$role->id}}", "POST", "", ["#parent_id"], ["{{$role->id}}"], ["id"]);
    })
</script>
</body>
</html>
