<!DOCTYPE html>
<html lang="en" class="px-2">

<head>
    <title>ویرایش موجودیت | نت افراز</title>
    @include('assets.styles')
</head>
<body id="app-container" class="menu-default">
@include('assets.menu')
<main>
    <div class="container-fluid">
        <div class="row" dir="rtl">
            <div class="col-12">
                <h1>ویرایش موجودیت</h1>
                <nav class="breadcrumb-container d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                    <ol class="breadcrumb pt-0">
                    </ol>
                </nav>
                <div class="separator mb-5">
                </div>
            </div>
            <div class="col-md-7 mx-auto ">
                <form autofill class="col-md-12">
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <label class="form-group has-float-label">
                                <input class="form-control" value="{{$entity->title}}" id="title">
                                <span>عنوان موجودیت</span>
                            </label>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label class="form-group has-float-label">
                                <input class="form-control" value="{{$entity->description}}" id="description">
                                <span>توضیحات</span>
                            </label>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label class="form-group has-float-label">
                                <input class="form-control" value="{{$entity->slug}}" id="slug">
                                <span>اسلاگ</span>
                            </label>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label class="form-group has-float-label">
                                <select class="form-control select2" id="parent_id">
                                    <option value="">بدون پدر</option>
                                    @foreach(\App\TicketEntity::getAll() as $en)
                                        <option value="{{$en->id}}">{{$en->title}}</option>
                                    @endforeach
                                </select>
                                <span>پدر</span>
                            </label>
                        </div>

                        <button class="col-md-2 btn btn-primary mt-2 mx-auto submit" type="submit">ویرایش موجودیت
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</main>
@include('assets.scripts')
<script>
    $(".submit").click(function (e) {
        e.preventDefault()
        submiter(["#title", "#slug"], "entity/edit/{{$entity->id}}", "POST", "", ["#description", "#parent_id"], ["{{$entity->id}}"], ["id"]);
    })
    $("#parent_id").val("{{$entity->parent_id}}").trigger("change")
</script>
</body>
</html>
