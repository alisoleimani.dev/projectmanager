<!DOCTYPE html>
<html lang="en">

<head>
    <title>نت افراز | مدیریت موجودیت ها</title>
    @include('assets.styles')
</head>

<body id="app-container" class="menu-default">
@include('assets.menu')
<main>
    <div class="col-12">
        <h1>مدیریت موجودیت ها</h1>
        <nav class="breadcrumb-container d-sm-block d-lg-inline-block" aria-label="breadcrumb">
            <ol class="breadcrumb pt-0">

            </ol>
        </nav>
        <div class="separator mb-5">
        </div>
    </div>
    <div class="col-md-12">
        <div class="table-responsive">
            <table class="table table-hover col-md-12" dir="rtl">
                <thead>
                <tr>
                    <th>ردیف</th>
                    <th>عنوان موجودیت</th>
                    <th>توضیحات</th>
                    <th>اسلاگ</th>
                    <th>پدر</th>
                    <th>مدیریت</th>
                </tr>
                </thead>
                @foreach($entitys as $entity)
                    <tr class="w-100">
                        <td class="w-10">{{$entity->id}}</td>
                        <td class="w-10">{{$entity->title}}</td>
                        <td class="w-30">{{$entity->description}}</td>
                        <td class="w-10">{{$entity->slug}}</td>
                        <td class="w-20">{{$entity->parentName()}}</td>
                        <td><a href="{{url('')}}/entity/edit/{{$entity->id}}">ویرایش</a></td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
</main>
@include('assets.scripts')
</body>
<script>

</script>

</html>
