@include('assets.styles',["title"=>__("Create",["model"=>__("User")])])

@include('assets.menu')
<div class="content-wrapper">
    <div class="row">
        <div class="col-12">
            <h1 class="">{{__("Create",["model"=>__("User")])}}</h1>
            <nav class="breadcrumb-container d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                <ol class="breadcrumb pt-0">
                </ol>
            </nav>
            <div class="separator mb-5">
            </div>
        </div>
        <div class="col-md-7 mx-auto ">
            <form autofill class="col-md-12">
                <div class="row">
                    <div class="col-md-12 mb-3">
                        <input class="form-control" id="name" placeholder="نام و نام خانوادگی">
                    </div>
                    <div class="col-md-6 mb-3">
                        <select class="form-control" id="role_id">
                            <option value="">لطفا انتخاب نمایید</option>
                            @foreach(\App\Role::getAll() as $role)
                                <option value="{{$role->id}}">{{$role->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-md-6 mb-3">
                        <input class="form-control" id="telegram" autocomplete="off" autofill="off"
                               placeholder="تلگرام">
                    </div>
                    <div class="col-md-6 mb-3">
                        <input class="form-control" id="email" autocomplete="nope" placeholder="ایمیل">
                    </div>
                    <div class="col-md-6 mb-3">
                        <input class="form-control" type="password" id="password" autocomplete="new-password"
                               placeholder="کلمه عبور">
                    </div>
                    <button class="col-md-2 btn btn-primary mt-2 mx-auto submit" type="submit">ایجاد کاربر</button>
                </div>
            </form>
        </div>
    </div>
</div>
</main>
@include('assets.scripts')
<script>
    $("#role_id").select2();
    $(".submit").click(function (e) {
        e.preventDefault()
        submiter(["#name", "#email", "#password", "#role_id"], "user/store", "POST", "", ["#telegram"]);
    })
</script>

