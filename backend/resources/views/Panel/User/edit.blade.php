<!DOCTYPE html>
<html lang="en">

<head>
    <title>نت افراز | ویرایش پروفایل</title>
    @include('assets.styles')
</head>

<body id="app-container" class="menu-default">
@include('assets.menu')
<main>
    <div class="container_fluid">
        <div class="row">
            <div class="col-md-12" dir="rtl">
                <div class="card d-flex flex-row mb-4">
                    <a class="d-flex" href="#">
                        <img alt="Profile" src="{{$user->avatar()}}"
                             class="img-thumbnail profile-pic border-0 rounded-circle m-4 list-thumbnail align-self-center small">
                    </a>
                    <div class="d-flex flex-grow-1 min-width-zero">
                        <div
                            class="card-body pl-0 align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero">
                            <div class="min-width-zero">
                                <a href="">
                                    <p class="list-item-heading mb-1 truncate">{{$user->name}}</p>
                                </a>
                                <p class="mb-2 text-muted text-small"></p>
                                <input type="file" id="avatar" style="display:none">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card p-md-5 p-2 col-md-12" style="min-height: 250px;">
            <div class="row mt-3 mr-md-2" dir="rtl">
                <div class="col-md-6 mt-1">
                    <label class="form-group has-float-label mb-4">
                        <input class="form-control" type="text" name="name" id="name" value="{{$user->name}}">
                        <span>نام و نام خانوادگی</span>
                    </label>
                </div>

                <div class="col-md-6 mt-1">
                    <label class="form-group has-float-label mb-4">
                        <input class="form-control" type="text" name="email" id="email"
                               value="{{$user->email}}">
                        <span>ایمیل</span>
                    </label>
                </div>

                <div class="col-md-6 mt-1">
                    <div class="input-group mb-2">
                        <div class="input-group-append">
                            <span class="input-group-text">تلگرام</span>
                        </div>
                        <input type="text" class="form-control" dir="ltr" value="{{$user->telegram}}"
                               id="telegram" placeholder="">
                        <div class="input-group-prepend">
                            <div class="input-group-text">@</div>
                        </div>

                    </div>
                </div>

            </div>
            <button type="submit" class="col-md-2 mt-3 btn btn-primary mx-auto" id="change-profile">بروزرسانی پروفایل
            </button>

        </div>
    </div>
</main>
@include('assets.scripts')
<script>
    $("#avatar-btn").click(function () {
        $("#avatar").click()
    })

    $("#change-profile").click(function (e) {
        e.preventDefault()
        submiter(["#name", "#email", "#telegram"], "user/edit/{{$user->id}}", "POST", "", "", ["{{$user->id}}"], ["id"]);
    })
</script>
</body>

</html>
