@include('assets.styles',["title"=>__("Create",["model"=>__("User")])])

@include('assets.menu')
<div class="content-wrapper">
    <div class="row">
        <div class="col-12">
            <h1 class="">{{__("Create",["model"=>__("User")])}}</h1>
            <nav class="breadcrumb-container d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                <ol class="breadcrumb pt-0">
                </ol>
            </nav>
            <div class="separator mb-5">
            </div>
        </div>
        <div class="container_fluid">
            <div class="row">
                <div class="col-md-12" dir="rtl">
                    <div class="card d-flex flex-row mb-4">
                        <a class="d-flex" href="#">
                            <img alt="Profile" src="{{$user->avatar()}}"
                                 class="img-thumbnail border-0 rounded-circle m-4 list-thumbnail align-self-center small">
                        </a>
                        <div class="d-flex flex-grow-1 min-width-zero">
                            <div
                            class="card-body pl-0 align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero">
                            <div class="min-width-zero">
                                <a href="">
                                    <p class="list-item-heading mb-1 truncate">{{$user->name}}</p>
                                </a>
                                <p class="mb-2 text-muted text-small"></p>
                                <button class="btn btn-primary px-3" id="edit">ویرایش پروفایل</button>
                                <input type="file" id="avatar" style="display:none">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card p-md-5 p-2 col-md-12" style="min-height: 250px;">
            <div class="row mt-3 mr-md-2" dir="rtl">
                <div class="col-md-6 mt-1">
                    <label class="form-group has-float-label mb-4">
                        <input class="form-control" type="text" disabled name="name" id="name" value="{{$user->name}}">
                        <span>نام و نام خانوادگی</span>
                    </label>
                </div>

                <div class="col-md-6 mt-1">
                    <label class="form-group has-float-label mb-4">
                        <input class="form-control" type="text" disabled name="email" id="email"
                               value="{{$user->email}}">
                        <span>ایمیل</span>
                    </label>
                </div>
                <div class="col-md-6 mt-1">
                    <div class="input-group mb-2">
                        <div class="input-group-append">
                            <span class="input-group-text">تلگرام</span>
                        </div>
                        <input type="text" class="form-control" disabled dir="ltr" value="{{$user->telegram}}"
                               name="telegram" placeholder="">
                        <div class="input-group-prepend">
                            <div class="input-group-text">@</div>
                        </div>
                    </div>
                </div>
            </div>
            <hr>

        </div>
    </div>
</main>
@include('assets.scripts')
@isset($myProfile)
    <script>
        $('#edit').click(function () {
            window.location.replace(url + 'user/profile/edit')
        });
    </script>
@else
    <script>
        $('#edit').click(function () {
            window.location.replace(url + 'user/edit/{{$user->id}}')
        });
    </script>
@endisset
</body>

</html>
