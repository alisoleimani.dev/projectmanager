<!DOCTYPE html>
<html lang="en">

<head>
    <title>نت افراز | مدیریت کاربران</title>
    @include('assets.styles')
</head>

<body id="app-container" class="menu-default">
@include('assets.menu')
<main>
    <div class="col-12">
        <h1>مدیریت کاربران</h1>
        <nav class="breadcrumb-container d-sm-block d-lg-inline-block" aria-label="breadcrumb">
            <ol class="breadcrumb pt-0">
            </ol>
        </nav>
        <div class="separator mb-5">
        </div>
    </div>
    <div class="col-md-12">
        <div class="table-responsive">
            <table class="table table-hover col-md-12" dir="rtl">
                <thead>
                <tr>
                    <th>ردیف</th>
                    <th>نام و نام خانوادگی</th>
                    <th>دپارتمان</th>
                    <th>ایمیل</th>
                    <th>تلگرام</th>
                    <th>مشاهده</th>
                </tr>
                </thead>
                @foreach($users as $user)
                    <tr class="w-100">
                        <td class="w-10">{{$user->id}}</td>
                        <td class="w-20">{{$user->name}}</td>
                        <td class="w-10">{{$user->role->name}}</td>
                        <td class="w-20"> {{$user->email}}</td>
                        <td class="w-20"> {{$user->telegram}}</td>
                        <td><a href="{{url('')}}/user/show/{{$user->id}}">مشاهده کاربر</a></td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
</main>
@include('assets.scripts')
</body>
</html>
