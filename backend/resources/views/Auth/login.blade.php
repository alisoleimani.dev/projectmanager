@include('assets.styles',["title"=>__("Login")])

<div class="container">

    <form action="index.html">
        <div class="row justify-content-md-center">
            <div class="col-xl-4 col-lg-5 col-md-6 col-sm-12">
                <div class="login-screen">
                    <div class="login-box">
                        <a href="#" class="login-logo">
                            <img src="@publicUrl/img/logo-dark.png" class="" alt="{{__("Identity")}}"/>
                        </a>
                        <h5>{{__("PleaseLogin")}}</h5>
                        <div class="form-group">
                            <input type="text" class="form-control" id="email" placeholder="{{__("Email")}}"/>
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" id="password" placeholder="{{__("Password")}}"/>
                        </div>
                        <div class="actions mb-4">
                            <button type="submit" class="btn btn-primary" id="login">{{__("Login")}}</button>
                        </div>
                        <div class="forgot-pwd">
                            <a class="link" href="forgot-pwd.html">{{__("ForgotPassword")}}</a>
                        </div>
                        <hr>
                        {{--                        <div class="actions align-left">--}}
                        {{--                            <span class="additional-link">New here?</span>--}}
                        {{--                            <a href="signup.html" class="btn btn-dark">Create an Account</a>--}}
                        {{--                        </div>--}}
                    </div>
                </div>
            </div>
        </div>
    </form>

</div>
@include('assets.scripts')
<script>
    $('#login').click(function (e) {
        e.preventDefault();
        submiter(["#email", "#password"], 'login', 'POST')
    });
    $("body").css({
        display: "flex",
        "align-items": "center",
        "justify-content": "center"
    });
    $("footer").hide()
</script>

