
<!DOCTYPE html>
<html lang="en" class="px-2">

<head>
    <title>{{$title}} | CRM</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Aliakbar Soleimani Crm Core">
    <meta name="author" content="ParkerThemes">
    <link rel="shortcut icon" href="@publicUrl/img/fav.png"/>
    <link rel="stylesheet" href="@publicUrl/css/apps.css">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css" rel="stylesheet"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.css"
          integrity="sha256-2bAj1LMT7CXUYUwuEnqqooPb1W0Sw0uKMsqNH0HwMa4=" crossorigin="anonymous"/>
    <style>
        h1,
        .form-group,
        .form-group input,
        select {
            text-align: {{\App\Helpers\I18nHelper::textAlign()}}  !important;
            direction: {{\App\Helpers\I18nHelper::langDirection()}};
        }

        .lang-dir {
            direction: {{\App\Helpers\I18nHelper::langDirection()}};
        }
    </style>
    {{\App\Facades\Hook::runhook(null,"adminHeader")}}
</head>
<body>
