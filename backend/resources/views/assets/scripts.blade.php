</div>
<footer class="main-footer">© Aliakbar Soleimani 2020</footer>
</div>

<script>
    var token = "{{csrf_token()}}";
    var url = "{{url('/')}}/";

</script>
<script src="@publicUrl/js/apps.js"></script>
<script src="@publicUrl/js/select2.full.js"></script>

<script src="@publicUrl/js/moment.js"></script>

<script src="@publicUrl/js/toastr.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.js"
        integrity="sha256-7OUNnq6tbF4510dkZHCRccvQfRlV3lPpBTJEljINxao=" crossorigin="anonymous"></script>
<script src="@publicUrl/js/submit.js"></script>
</body>
</html>
<script>
    $(".select2").select2();
    //selectize
</script>
