<?php

namespace Module\Shop;

use App\Classes\ModuleAbstract;
use App\Traits\ModuleViewTrait;
use Illuminate\Support\Facades\Route;
use function App\Classes\views;

class Main extends ModuleAbstract
{
    use ModuleViewTrait;

    public function __construct()
    {
        Route::get("/s", function () {
            return self::view("hi");
        });

        \App\Facades\Menu::addMenu("Shop", "My Shop");

    }

    public static function subMenu(): array
    {
        return [
            "index" => ["uri" => "shop", "slug" => "shop"],
            "create" => ["uri" => "shop", "slug" => "Add Product"]
        ];
    }

    public static function dir()
    {
        return base_path("modules/shop");
    }

    public function activate()
    {

    }

    public function deActivate()
    {

    }

    public function index()
    {

    }
}
