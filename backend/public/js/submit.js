function ajax(uri, data, method) {
    return $.ajax({
        type: method,
        url: url + uri,
        dataType: "script",
        cache: !1,
        contentType: !1,
        processData: !1,
        data: data,
        success: function (result) {
            console.log(result);
            result = JSON.parse(result);
            if (result.status) {
                if (result.action) {
                    if (result.action == "REFRESH") {
                        location.reload();
                        return;
                    }
                    if (result.action == "REDIRECT") {
                        window.location.replace(url + result.url);
                        return;
                    }
                    if (result.action == "ACTION_ALERT") {
                        if (result.mode == "swal") {
                            swal.fire("", result.msg, result.color);
                            $(".confirm").click(function () {
                                window.location.replace(url + result.url);
                            });
                            return;
                        } else if (result.mode == "toastr") {
                            toastr[result.color](result.msg);
                            setTimeout(function () {
                                window.location.replace(url + result.url);
                            }, 800)
                            return;
                        }
                    }
                }
                swal.fire("", result.msg, "success");
            } else {
                swal.fire("", result.msg, "error");
            }
            return result;
        },
        beforeSend: function () {
            $(".unclick-modal").css("display", "flex"),
                $(".loader2").css("display", "flex");
        },
        complete: function () {
            $(".unclick-modal").hide(), $(".loader2").hide();
        },
        error: function (error) {
            console.log(error), toastr.error("مشکل در اتصال به سرور");
        }

    });
}

toastr.options = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": true,
    "progressBar": true,
    "positionClass": "toast-bottom-left",
    "preventDuplicates": true,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "2000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
}

function validate(element) {
    var val;
    return "" == $(element).val()
        ? ($(element).addClass("is-invalid"),
            setTimeout(function () {
                $(element).removeClass("is-invalid");
            }, 3000),
        element + "/")
        : "";
}
function validator(fields) {
    for (var c = fields.length, i = 0, data = ""; i < c;)
        (data += validate(fields[i])), i++;
    return (
        "" == data ||
        (toastr.error("لطفا فیلد های اجباری را تکمیل نمایید"), data)
    );
}
function ajaxdata(fields, values) {
    var form_data = new FormData();
    form_data.append("_token", token);
    for (var c = fields.length, i = 0; i < c;) {
        var field = fields[i].substr(1);
        form_data.append(field, values[i]), i++;
    }
    return form_data;
}
function formwizard(fields) {
    if (1 == validator(fields)) {
        for (var c = fields.length, i = 0, values = []; i < c;) {
            var val = $(fields[i]).val();
            values.push(val), i++;
        }
        var data;
        return ajaxdata(fields, values);
    }
    return !1;
}

function submiter(fields, url, method, files, optional, vars, keys, autoResponse = true) {
    var wizard = formwizard(fields);
    if (0 != wizard) {
        if (Array.isArray(optional))
            for (var c = optional.length, i = 0; i < c;) {
                var val = $(optional[i]).val(),
                    field = optional[i].substr(1);
                wizard.append(field, val), i++;
            }
        if (Array.isArray(files))
            for (var c = files.length, i = 0; i < c;) {
                var val = $(files[i]).prop("files")[0],
                    field = files[i].substr(1);
                wizard.append(field, val), i++;
            }
        if (Array.isArray(vars))
            for (var c = vars.length, i = 0; i < c;) {
                var field = keys[i];
                wizard.append(field, vars[i]), i++;
            }
        return ajax(url, wizard, method);
    }
    return !1;
}
