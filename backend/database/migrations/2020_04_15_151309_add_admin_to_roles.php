<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAdminToRoles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('roles', function (Blueprint $table) {
            $table->unsignedBigInteger("admin_id")->after("parent_id")->nullable();
            $table->foreign("admin_id")->references("id")->on("users")->onDelete("SET NULL");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('roles', function (Blueprint $table) {
            $table->unsignedBigInteger("admin_id")->after("parent_id")->nullable();
            $table->foreign("admin_id")->references("id")->on("users")->onDelete("SET NULL");
        });
    }
}
