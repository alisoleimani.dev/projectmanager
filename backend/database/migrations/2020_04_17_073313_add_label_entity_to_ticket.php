<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLabelEntityToTicket extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tables', function (Blueprint $table) {
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tickets', function (Blueprint $table) {
            $table->unsignedBigInteger("entity_id")->after("user_id");
            $table->unsignedBigInteger("label_id")->after("entity_id");
            $table->softDeletes();
            $table->foreign("entity_id")->references("id")->on("entities");
            $table->foreign("label_id")->references("id")->on("labels");
        });
    }
}
